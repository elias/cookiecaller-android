package de.wagners.wurscht;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Sender{
    DatagramSocket sender;

    SocketAddress address;

    DatagramPacket nachrichtenPacket;
    byte[] nachricht;

    DatagramPacket antwortPacket;
    byte[] antwort;

    public Sender(Inet4Address addr, int port){
        address = new InetSocketAddress(addr, port);

        try {
            sender = new DatagramSocket(port);
            sender.setBroadcast(true);
            //sender.setSoTimeout(5000);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        nachricht = new byte[156];
        nachrichtenPacket = null;

        antwort = new byte[16];
        antwortPacket = new DatagramPacket(antwort, antwort.length, address);
    }

    public void senden(String _name, String _nachricht){
        nachricht = bauePacket(_name, _nachricht);
        nachrichtenPacket = new DatagramPacket(nachricht, nachricht.length, address);

        try {
            System.out.println("Nachricht '" + _nachricht + "' wird gesendet... ");
            MainActivity.output.setText("Senden...");
            sender.send(nachrichtenPacket);

            System.out.println("Nachricht gesendet. Warten auf Antwort...");
            MainActivity.output.setText("Warte auf Antworten...");

            String outputInhalt;
            while(true) {
                sender.receive(antwortPacket);
                if(antwort[0] == 1) {
                    outputInhalt = MainActivity.output.getText().toString() + "\n" + getName(antwort) + " hat die Nachricht erhalten";
                    MainActivity.output.setText(outputInhalt);
                    System.out.println(outputInhalt);
                }
            }

        } catch (IOException e) {
            System.out.println("Senden Fehlgeschlagen!");
            throw new RuntimeException(e);
        }
    }

    private byte[] bauePacket(String name, String nachricht){
        byte[] packet = new byte[160];

        packet[0] = 0;

        byte[] namenByte = name.getBytes(StandardCharsets.UTF_8);
        namenByte = Arrays.copyOf(namenByte, 15);
        for(int i = 1; i<= 15; i++){
            packet[i] = namenByte[i-1];
        }

        byte[] nachrichtenByte = nachricht.getBytes(StandardCharsets.UTF_8);
        nachrichtenByte = Arrays.copyOf(nachrichtenByte, 140);
        for(int i = 16; i<= 155; i++){
            packet[i] = nachrichtenByte[i-16];
        }

        return packet;
    }

    private String getName(byte[] _antwort){
        byte[] namenByte = new  byte[15];

        for(int i = 1; i <= 15; i++){
            namenByte[i-1] = _antwort[i];
        }

        String name = new String(namenByte, StandardCharsets.UTF_8);
        name = name.trim();

        return name;
    }

    public String getEmpfaengerName(){
        return getName(antwort);
    }
}