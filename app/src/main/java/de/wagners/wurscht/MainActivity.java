package de.wagners.wurscht;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    public Sender sender;
    public EditText textInput;
    public static TextView output;
    public EditText nameInput;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.setProperty("java.net.preferIPv4Stack", "true");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.toolbar_items);

        // Hier startet eigener Code
        Button sendenButton = (Button) findViewById(R.id.button);
        textInput = (EditText) findViewById(R.id.textInput);
        output = (TextView) findViewById(R.id.textView);
        nameInput = (EditText) findViewById(R.id.nameInput);

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("preferences", 0);
        SharedPreferences.Editor editor = preferences.edit();
        nameInput.setText(preferences.getString("name", null));
        System.out.println(preferences.getString("name", null) + " ist der gespeicherte Name");

        try {
            sender = new Sender((Inet4Address) InetAddress.getByName("192.168.0.255"), 56789);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        sendenButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                editor.putString("name", nameInput.getText().toString());
                System.out.println("Settings gespeichert: " + editor.commit());

                if(!wifiManager.isWifiEnabled() || wifiManager.getDhcpInfo().ipAddress == 0){
                    sendenButton.performHapticFeedback(HapticFeedbackConstants.REJECT);

                    output.setText("Keine WLAN-Verbindung :/");
                }else {

                    sendenButton.performHapticFeedback(HapticFeedbackConstants.CONFIRM);

                    Thread senderThread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            String name = nameInput.getText().toString();
                            String nachricht = textInput.getText().toString();

                            if (name.length() == 0) {
                                name = "Anonym";
                            }
                            if (nachricht.length() == 0) {
                                nachricht = "Es gibt Essen!";
                            }

                            sender.senden(name, nachricht);
                        }
                    });

                    senderThread.start();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}